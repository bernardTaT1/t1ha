// ============================================================
// more-tests.c from CLion::t1ha
//
// Created by bernard on 18/08/18 at 01:12.
// ============================================================

#include "common.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const int loops = 10;
const int buffer_len = 256;
const uint64_t seed = ~UINT64_C(42);
static t1ha_context_t ctx;

static uint64_t exec_test(const int k) {
    char buffer[buffer_len];
    size_t size = (size_t )snprintf(buffer, buffer_len, "Ligne %d", k);

    t1ha2_update(&ctx, buffer, size);
    return t1ha2_final(&ctx, NULL);
}

int main(int argc, const char *argv[]) {
    t1ha2_init(&ctx, seed, seed);
    for (int k=1; k<=loops; k++) {
        fprintf(stdout, "test %3d, key %016lx\n", k, exec_test(k));
    }
    return EXIT_SUCCESS;
}

